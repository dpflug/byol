CC=cc
CFLAGS=-std=c99 -Wall

all: $(basename $(wildcard *.c))

readline_prompt:: readline_prompt.c
	$(CC) $(CFLAGS) -lreadline $< -o $@

%:: %.c
	$(CC) $(CFLAGS) $< -o $@
